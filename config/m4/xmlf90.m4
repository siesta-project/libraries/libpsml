# -*- Autoconf -*-
#
# M4 macros for PSML
#
# Copyright (C) 2017 Yann Pouillon
#
# This file is part of the PSML software package. For license information,
# please see the COPYING file in the top-level directory of the PSML source
# distribution.
#

#
# XMLF90 support
#



# PSML_XMLF90_DETECT()
# --------------------
#
# Checks that the selected XMLF90 libraries properly work.
#
AC_DEFUN([PSML_XMLF90_DETECT],[
  dnl Init
  psml_xmlf90_ok="unknown"

  dnl Prepare environment
  saved_CPPFLAGS="${CPPFLAGS}"
  saved_FCFLAGS="${FCFLAGS}"
  saved_LIBS="${LIBS}"
  CPPFLAGS="${CPPFLAGS} ${psml_xmlf90_incs}"
  FCFLAGS="${FCFLAGS} ${CPPFLAGS}"
  LIBS="${psml_xmlf90_libs} ${LIBS}"

  dnl Check XMLF90 routine
  AC_LANG_PUSH([Fortran])
  AC_MSG_CHECKING([whether the XMLF90 library works])
  AC_LINK_IFELSE([AC_LANG_PROGRAM([],
    [[
      use xmlf90_sax
      character(len=80) :: fname
      type(xml_t) :: fxml
      integer :: iostat
      call open_xmlfile(fname,fxml,iostat)
      call close_xmlfile(fxml)
    ]])], [psml_xmlf90_ok="yes"], [psml_xmlf90_ok="no"])
  AC_MSG_RESULT([${psml_xmlf90_ok}])
  AC_LANG_POP([Fortran])

  dnl Restore environment
  CPPFLAGS="${saved_CPPFLAGS}"
  FCFLAGS="${saved_FCFLAGS}"
  LIBS="${saved_LIBS}"
]) # PSML_XMLF90_DETECT
