# ChangeLog for libPSML

## 1.1.12 (2022-11-16)

* Add CMake building framework

## 1.1.10 (2020-04-24)

* Remove dependency on iso_varying_string module, using allocatable
  character variables instead.

* Consolidate list of changes in ChangeLog.md

## 1.1.9 (2020-01-20)

* A dependency was missing in src/Makefile.am, leading to
  problems with threaded compilation (make -jN)
    
* Updated the description of the documentation workflow
     (regarding GitHub pages)
    
* Add CPC reference for Pseudo-Dojo

## 1.1.8 (2019-10-01)

* Simplify the declaration of EMPTY_ASSOC_LIST in the assoc_list module to help
  some compilers.

* Update release_notes, version.info, PSML reference, etc.

## 1.1.7 (2018-02-02)

* Udpate README and add some extra documentation

* Add examples/run_test.sh to the distribution

* Had the build system install a psml.mk file in org.siesta-project

## 1.1.6 (2017-12-01)

* The old API routines have been removed

* New autotools-based building system by Yann Pouillon

## 1.1.5 (2017-07-27) Release

* This version complies with the PSML 1.1 specification,
as detailed in doc/schema/psml.rnc.

* A new API, following closely the structure of the schema, has
been implemented. The old API routines are retained to ease the
transition of client codes to the new API.


<!--
Local Variables:
mode: fundamental
fill-column: 70
indent-tabs-mode: nil
coding: utf-8
End:
-->
