# -*- Autoconf -*-
#
# Configure script for the LibPSML package
#
# Copyright (C) 2017 Y. Pouillon
#
# This file is part of the LibPSML software package. For license information,
# please see the COPYING file in the top-level directory of the source
# distribution.
#

                    # ------------------------------------ #

#
# Autotools startup
#

# Init Autoconf
AC_PREREQ(2.69)
AC_INIT([LibPSML], [2.1.0], [https://gitlab.com/siesta-project/libraries/libpsml], [libpsml])
AC_REVISION([Autotools support for LibPSML])
AC_CONFIG_AUX_DIR(config/gnu)
AC_CONFIG_MACRO_DIR([config/m4])
AC_CONFIG_SRCDIR(src/m_psml.F90)
_AC_SRCDIRS([.])

# Init Automake
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE([1.14 tar-pax])
AM_CONFIG_HEADER([config.h])

# Generate timestamp
psml_timestamp=`date '+%Y%m%dT%H%M%S%z'`
AC_SUBST(psml_timestamp)

                    # ------------------------------------ #

#
# System utilities
#

# Check for common programs
AC_PROG_MAKE_SET
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_SED
AC_PROG_AWK
AC_PROG_GREP

                    # ------------------------------------ #

#
# Default settings
#

# Note for developers: you may edit these parameters to tune the behaviour
# of the build system. Please do it with care.

# Lists of options to manage (psml_*_enable_def variables)
#
# Note:
#   * switches (--enable-*) must be set to "yes" or "no"
#   * external dependencies (--with-*) must be set to "yes", "no", or "auto"
#
psml_cfg_switches="procptrs"
psml_cfg_extdeps="xmlf90"

# Optional features
psml_procptrs_enable_def="yes"

# XMLF90
psml_xmlf90_enable_def="yes"
psml_xmlf90_incs_def=""
psml_xmlf90_libs_def="-lxmlf90"

                    # ------------------------------------ #

#
# Check consistency of defaults
#

# Default triggers for optional features must be yes or no, and not empty
for psml_cfg_default in ${psml_cfg_switches}; do
  tmp_default=`eval echo \$\{psml_${psml_cfg_default}_enable_def\}`
  if test "${tmp_default}" != "no" -a \
          "${tmp_default}" != "yes"; then
    AC_MSG_ERROR([invalid default value: psml_${psml_cfg_default}_enable_def = ${tmp_default}])
  fi
done

# Default triggers for external dependencies must be yes, no, or auto
for psml_cfg_default in ${psml_cfg_extdeps}; do
  tmp_default=`eval echo \$\{psml_${psml_cfg_default}_enable_def\}`
  AC_MSG_CHECKING([default detection behavior for ${psml_cfg_default}])
  AC_MSG_RESULT([${tmp_default}])
  if test "${tmp_default}" != "" -a \
          "${tmp_default}" != "auto" -a \
          "${tmp_default}" != "no" -a \
          "${tmp_default}" != "yes"; then
    AC_MSG_ERROR([invalid default value: psml_${psml_cfg_default}_enable_def = ${tmp_default}])
  fi
done

# Clean-up
unset tmp_default

                    # ------------------------------------ #

#
# Optional features
#

# Conditional compilation (alphabetical order)
AC_ARG_ENABLE([procptrs],
  [AS_HELP_STRING([--enable-procptrs],
    [Enable procedure pointers (default: ${psml_procptrs_enable_def})])],
  [psml_procptrs_enable="${enableval}"; psml_procptrs_type="yon"],
  [psml_procptrs_enable="${psml_procptrs_enable_def}"; psml_procptrs_type="def"])
AC_SUBST(enable_procptrs)

                    # ------------------------------------ #

#
# External dependencies
#

# XMLF90 (mandatory)
AC_ARG_WITH([xmlf90],
  [AS_HELP_STRING([--with-xmlf90],
    [Install prefix of the XMLF90 libraries (e.g. '/usr/local'), or comma-separated compilation directives for includes and libraries (e.g. '-I/usr/include,-L/usr/lib64 -lxmlf90').])],
  [ if test "${withval}" = "no" -o "${withval}" = "yes"; then
      psml_xmlf90_enable="${withval}"
      psml_xmlf90_type="yon"
      if test "${withval}" = "yes"; then
        psml_xmlf90_incs="${psml_xmlf90_incs_def}"
        psml_xmlf90_libs="${psml_xmlf90_libs_def}"
      fi
    else
      psml_xmlf90_enable="yes"
      tmp_comma=`echo "${withval}" | grep ','`
      if test "${tmp_comma}" = ""; then
        psml_xmlf90_type="pfx"
        psml_xmlf90_incs="-I${withval}/include"
        psml_xmlf90_libs="-L${withval}/lib ${psml_xmlf90_libs_def}"
      else
        psml_xmlf90_type="csd"
        psml_xmlf90_incs="`echo ${withval} | cut -d',' -f1`"
        psml_xmlf90_libs="`echo ${withval} | cut -d',' -f2`"
      fi
    fi],
  [psml_xmlf90_enable="${psml_xmlf90_enable_def}"; psml_xmlf90_type="def";
    psml_xmlf90_incs="${psml_xmlf90_incs_def}"; psml_xmlf90_libs="${psml_xmlf90_libs_def}"])
AC_SUBST(with_xmlf90)

                    # ------------------------------------ #

#
# Check option consistency
#

# All --enable-* options must be yes or no
for psml_cfg_option in procptrs; do
  tmp_option=`eval echo \$\{enable_${psml_cfg_option}\}`
  if test "${tmp_option}" != "" -a \
          "${tmp_option}" != "no" -a \
          "${tmp_option}" != "yes"; then
    AC_MSG_ERROR([--enable-${psml_cfg_option} must be "yes" or "no"])
  fi
done

# XMLF90 support is mandatory
if test "${psml_xmlf90_enable}" = "no"; then
  AC_MSG_WARN([XMLF90 is mandatory but its search parameters are not set])
fi

# Improve over default values when possible
if test "${psml_xmlf90_type}" = "def"; then
  if test -n "$EBROOTXMLF90"; then
    AC_MSG_NOTICE([using EasyBuild information to look for XMLF90])
    psml_xmlf90_type="env"
    psml_xmlf90_incs="-I${EBROOTXMLF90}/include"
    psml_xmlf90_libs="-L${EBROOTXMLF90}/lib ${psml_xmlf90_libs_def}"
  fi
fi

# Clean-up
unset tmp_option

                    # ------------------------------------ #

#
# Substitute build-system variables
#

# Defaults
AC_SUBST(psml_procptrs_enable_def)
AC_SUBST(psml_xmlf90_enable_def)
AC_SUBST(psml_xmlf90_incs_def)
AC_SUBST(psml_xmlf90_libs_def)

# Triggers
AC_SUBST(psml_procptrs_enable)

# Initialization types
AC_SUBST(psml_procptrs_type)
AC_SUBST(psml_xmlf90_type)

# Build flags for external dependencies
AC_SUBST(psml_xmlf90_incs)
AC_SUBST(psml_xmlf90_libs)

                    # ------------------------------------ #

#
# C language support
#

# Init C compiler and preprocessor
AC_PROG_CC
AC_PROG_CPP

# C compiler peculiarities (for Libtool)
AM_PROG_CC_C_O

                    # ------------------------------------ #

#
# Fortran language support
#

# Look for the Fortran compiler
if test "${FC}" != "" -a ! -x "${FC}"; then
  psml_fc_probe=`echo "${FC}" | sed -e 's/ .*//'`
  if test ! -x "${psml_fc_probe}"; then
    AC_PATH_PROG([psml_fc_path],[${psml_fc_probe}])
    if test "${psml_fc_path}" = ""; then
      AC_MSG_ERROR([could not run Fortran compiler "${FC}"])
    fi
  fi
fi
AC_PROG_FC

# Fail if no Fortran compiler is available
if test "${FC}" = ""; then
  AC_MSG_ERROR([no Fortran compiler available])
fi


# Fortran compiler peculiarities
AX_F90_MODULE_EXTENSION
AC_SUBST(ax_cv_f90_modext)
AX_F90_MODULE_CASE

# Select how to handle Fortran modules
AM_CONDITIONAL([F90_MOD_UPPERCASE],
  [test "${ax_cv_f90_mod_uppercase}" = "yes"])

                    # ------------------------------------ #

#
# Libtool configuration
#

# Init Libtool (must be done once compilers are fully set)
LT_INIT
LT_PREREQ([2.4.2])
LT_LANG([Fortran])
LTOBJEXT="lo"
AC_SUBST(LTOBJEXT)

                    # ------------------------------------ #

#
# External dependencies
#

# XMLF90
AC_MSG_CHECKING([how XMLF90 parameters have been set])
AC_MSG_RESULT([${psml_xmlf90_type}])
AC_MSG_CHECKING([for XMLF90 include flags])
AC_MSG_RESULT([${psml_xmlf90_incs}])
AC_MSG_CHECKING([for XMLF90 library flags])
AC_MSG_RESULT([${psml_xmlf90_libs}])
PSML_XMLF90_DETECT
if test "${psml_xmlf90_ok}" = "yes"; then
  CPPFLAGS="${CPPFLAGS} ${psml_xmlf90_incs}"
  FCFLAGS="${FCFLAGS} ${psml_xmlf90_incs}"
  LIBS="${psml_xmlf90_libs} ${LIBS}"
else
  AC_MSG_FAILURE([XMLF90 is missing or incomplete])
fi

                    # ------------------------------------ #

#
# Adjust build parameters according to configure options
#

# Conditional compilation (alphabetical order)
if test "${psml_procptrs_enable}" = "no"; then
  AC_DEFINE([__NO_PROC_POINTERS__], 1,
    [Define to 1 if you want to disable procedure pointers (for oldish compilers).])
fi

                    # ------------------------------------ #

# Write down YAML configuration now (allows full report with deferred errors)
AC_OUTPUT([config/data/libpsml-config.yml])

# Report configuration
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Final build parameters])
AC_MSG_NOTICE([----------------------])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([TSTAMP     = ${psml_timestamp}])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([CPP        = ${CPP}])
AC_MSG_NOTICE([CPPFLAGS   = ${CPPFLAGS}])
AC_MSG_NOTICE([CC         = ${CC}])
AC_MSG_NOTICE([CFLAGS     = ${CFLAGS}])
AC_MSG_NOTICE([FC         = ${FC}])
AC_MSG_NOTICE([FCFLAGS    = ${FCFLAGS}])
AC_MSG_NOTICE([LDFLAGS    = ${LDFLAGS}])
AC_MSG_NOTICE([LIBS       = ${LIBS}])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Enable procedure pointers: ${psml_procptrs_enable} (init: ${psml_procptrs_type})])
AC_MSG_NOTICE([])

                    # ------------------------------------ #

#
# Output configuration
#

AC_OUTPUT([
  Makefile
  config/data/libpsml.pc
  config/data/psml.mk
  doc/Makefile
  examples/Makefile
  src/Makefile
])
