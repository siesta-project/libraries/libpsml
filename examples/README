Example programs and psml files

Example programs:

* normalize:  Parses a PSML file and dumps the resulting `ps` object.

* show_psml: A program to parse a PSML file and extract
  various kinds of information, with optional evaluation of radial
  functions on a linear grid for later plotting.

* getz: A very simple example of extraction of a single piece of data


The psml files in this directory (which are not meant for production
work) have been created in a number of ways:

(ATOM is the pseudopotential generator formerly
 distributed with Siesta.
 See http://siesta-project.org/siesta/Pseudopotentials/)

(ONCVPSP is Don Hamann's pseudopotential generator.
 Versions of ONCVPSP able to produce PSML files are available in
[Gitlab](https://gitlab.com/garalb/oncvpsp-psml/-/releases).)

(PSOP is a Vlocal and KB projector generator that
 uses Siesta's classic approach. It is available in the master
 version of Siesta.)

Ba.sc-ionic.psml :  ATOM (dirac, sr+so semilocal pots)
Fe.spin.psml     :  ATOM (spin-pol, spin ave+diff sl pots)


Ba.sc-ionic-siesta-vnl.psml :
Si.tm2.nrl.vnl.psml:
Fe.spin-siesta-vnl.psml:
                                 ATOM + Siesta's PSOP
                               (addition of projectors and Vlocal)

14_Si.psml       :  ONCVPSP (scalar-relativistic)
                            (with wavefunctions, '-w' option)
14_Si_UPF_r.psml :  ONCVPSP (dirac, with lj-style projs)
                            (with wavefunctions, '-w' option)
52_Te_r.psml     :  ONCVPSP (dirac, with sr+so projs)
80_Hg.psml       :  ONCVPSP (scalar-relativistic)
83_Bi_r.psml     :  ONCVPSP (dirac, with sr+so AND lj projs)
                            (with command-line option '-r both')
			    
80_Hg-siesta-vnl.psml: ONCVPSP + Siesta's PSOP
    (replacement of Vlocal and KB projectors by
     the Siesta flavored ones, built from the
     semilocal potentials in the ONCVPSP file)

You can use the show_psml program to get information:

./show_psml [options] File.psml
./show_psml -h   # for help
