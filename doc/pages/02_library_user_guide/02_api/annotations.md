title: Annotations

XML provides for built-in extensibility and client programs can use as
much or as little information as needed. For the actual mapping of a
domain ontology to a XML-based format, however, clients and producers
have to agree on the terms used. What has been described in the above
sections is a minimal form of such a mapping, containing the basic
concepts and functions needed. The extension of the format with new
fixed-meaning elements and attributes would involve an updated schema
and re-coding of parsers and other programs. A more light-weight
solution to the extensibility issue is provided by the use of
`annotations`, which have the morphology of XML empty elements
(containing only attributes) but can appear in various places and
contain arbitrary key-value pairs. Annotations provide immediate
information to human readers of the PSML files, and can be
exploited informally by client programs to extract additional
information. For the latter use, it is clear that some degree of
permanence and agreed meaning should be given to annotations, but this
task falls not on some central authority, but on specific codes.

As an example, the following snippet of a PSML file shows an annotation in the 
`pseudo-atom-spec` element:

```xml
<pseudo-atom-spec atomic-label="La" atomic-number="57" z-pseudo="11"
 flavor="Hamann&apos;s oncvpsp" relativity="scalar" spin-dft="no"
 core-corrections="no">
<annotation pseudo-energy="-31.7585823482" cutoff_hint_low="38"
 cutoff_hint_normal="39" cutoff_hint_high="47" />
<exchange-correlation>
<annotation oncvpsp-xc-code="3"
 oncvpsp-xc-type="LDA -- Ceperley-Alder Perdew-Zunger" />
<libxc-info number-of-functionals="2">
<functional name="Slater exchange (LDA)" type="exchange" id="1" />
<functional name="Perdew &amp; Zunger (LDA)" type="correlation" id="9" />
</libxc-info>
</exchange-correlation>
<valence-configuration total-valence-charge="11">
<shell n="5" l="s" occupation="2" />
<shell n="5" l="p" occupation="6" />
<shell n="5" l="d" occupation="2" />
<shell n="6" l="s" occupation="1" />
</valence-configuration>
</pseudo-atom-spec>
```

Plane-wave codes might want to exploit the energy cutoff hints given
in the annotation, perhaps using a `plugin routine` that identifies
the generator program and its version and extracts the hints in the
appropriate manner. These plugin routines might be stored (and
properly maintained) in a repository to which both the owners of the
generator programs and the owners of the client programs can
contribute. Some chosen plugins might also be distributed with the
libPSML library itself, to serve as examples and templates.

Annotations are currently allowed within the following elements:

* `provenance`
* `pseudo-atom-spec`
* `exchange-correlation`
* `valence-configuration`
* `core-configuration`
* `semilocal-potentials`
* `local-potential`
* `nonlocal-projectors`
* `pseudo-wave-functions`
* `valence-charge`
* `core-charge`
* `grid`

Top-level annotations are not allowed. They properly
belong in the `provenance` elements.
