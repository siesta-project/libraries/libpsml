title: First-principles codes able to read PSML files

On the client side, we have incorporated the libPSML library in SIESTA
and ABINIT. PSML files can then be directly read and processed by these
codes, achieving pseudopotential interoperability.

### SIESTA

The PSML functionality has been available in [SIESTA](https://siesta-project.org) since Version 5.0.

### ABINIT

The [Abinit code](https://www.abinit.org) has been able to process PSML files since Version 8.2.
