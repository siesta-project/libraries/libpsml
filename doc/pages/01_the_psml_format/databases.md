title: Databases of PSML files

The [Pseudo-Dojo](http://www.pseudo-dojo.org) project maintains a
database of pseudopotentials generated
with the oncvpsp code, and provides extensive information on their
performance on several tests. One of the formats supported is PSML.

More information about the project is available in a
[publication](https://doi.org/10.1016/j.cpc.2018.01.012) (also available
in [preprint form](https://arxiv.org/abs/1710.10138)).
